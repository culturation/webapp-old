class CreateContents < ActiveRecord::Migration[5.2]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :title_lang
      t.string :link
      t.text :description
      t.date :publication_date
      t.string :audio_lang
      t.string :subtitles_lang
      t.string :tags
      t.integer :duration

      t.timestamps
    end
  end
end
